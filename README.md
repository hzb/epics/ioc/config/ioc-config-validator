# How to validate your ioc_config.yml?

To make sure that we use a proper ioc_config.yml for the IOC deployment, we have to check the schema and whether the required modules are included in the .yml. 

In order to do so, a pipeline can be included in any of the config repos. To do so:

```
include:
  - project: 'hzb/epics/ioc/config/ioc-config-validator'
    ref: main
    file: 
      - .gitlab-ci.yml
```
The script is going to validate the schema and check for the required fields:

```
        "required": [
          "ioc_name",
          "ioc_source_repo",
          "ioc_source_tag",
          "epics_version", 
          "epics_image_tag"
        ]
```