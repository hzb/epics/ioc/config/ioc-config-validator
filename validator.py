import jsonschema
from jsonschema import Draft7Validator
import yaml
import json


# Load the schema from a file
with open('ioc_config_validator.json') as f:
  schema = json.load(f)

validator = Draft7Validator(schema)

# Load the YAML data
with open('../ioc_config.yml') as f:
  data = yaml.safe_load(f)

# Perform validation
try:
  mess = validator.validate(data, schema)
  print("Validation successful!")
except jsonschema.exceptions.ValidationError as e:
  print("Validation error:", e)
  exit(1)
except jsonschema.exceptions.SchemaError as e:
  print("Schema error:", e)
  exit(1)


